<?php
    include('libs/common.php'); 
	outputCMSNav();
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient =(new MongoDB\Client);
    $db =$mongoClient-> ecommerce;
    $productObj = $db->Products->findOne(['_id' => new MongoDB\BSON\ObjectId($_GET['id'])]); // Get product from database based on the ID in the URL
?>

<div class="cms-content">
    <div id="banner">
        <h1>EDIT PRODUCT</h1>
    </div>
    <!-- Edit product form -->
    <div class="form">
        <form action=<?php echo "edit_product_database.php?id=" . $_GET['id'] ?> method = post>
            <label for="name">Product name</label><br />
            <input type="text" id="name" name="name" value="<?php echo $productObj['name']?>"><br /><br />
            <label for="pic">Product picture</label><br />
            <input type="file" id="pic" name="pic"> <br /><br />
            <label for="weight">Weight (in kilograms)</label><br />
            <input type="number" id="weight" name="weight" value=<?php echo $productObj['weight']?>><br /><br />
            <label for="price">Price (in pounds)</label><br />
            <input type="number" id="price" name="price" value=<?php echo $productObj['price']?>><br /><br />
            <label for="quantity">Quantity</label><br />
            <input type="number" id="quantity" name="quantity" value=<?php echo $productObj['quantity']?>><br /><br />
            <label for="category">Category</label><br />
            <select name="category" id="category">
                <?php
                    if($productObj['category'] == "bread") { // Check category of the product
                        echo "<option value='bread' selected>Bread</option>
                        <option value='rolls'>Rolls</option>
                        <option value='other'>Other</option>";
                    }
                    elseif($productObj['category'] == "rolls") {
                        echo "<option value='bread'>Bread</option>
                        <option value='rolls' selected>Rolls</option>
                        <option value='other'>Other</option>";
                    }
                    else {
                        echo "<option value='bread'>Bread</option>
                        <option value='rolls'>Rolls</option>
                        <option value='other' selected>Other</option>";
                    }
                ?>
            </select><br /><br />
            <label for="tage">Product tags</label><br />
            <input type="text" id="tags" name="tags" value="<?php echo $productObj['tags']?>"/><br /><br />
            <input id="butt" type="submit" value="Save" /><br /><br />
        </form>
    </div>
</div>

<script>
    if(sessionStorage.loggedInUsr == undefined) { // Check if user is logged in
        window.location.replace("cms-login.php");
    }
</script>

<?php
    outputFooter();
?>