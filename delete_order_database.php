<?php
    // Delete order server
    include('libs/common.php'); 
    outputCMSNav();
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient = (new MongoDB\Client);
    $db = $mongoClient->ecommerce;
    $custID = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
    $deleteCriteria = [ // Get ID of the object from the URL
        "_id" => new MongoDB\BSON\ObjectID($_GET['id']) 
    ];
    $deleteRes = $db->Order->deleteOne($deleteCriteria);
    if($deleteRes->getDeletedCount() == 1){
        echo 'Order deleted successfully.';
    }
    else {
        echo 'Error deleting order';
    }
    outputFooter();
?>

