<?php
    // Edit order server
    include('libs/common.php'); 
    outputCMSNav();
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient =(new MongoDB\Client);
    $db =$mongoClient-> ecommerce;
    $collection =$db->Products;
    if($_POST)
    {
        $replace =  array(
            'name'=>$_POST['name'],
            'weight'=>$_POST['weight'],
            'price'=>$_POST['price'],
            'quantity'=>$_POST['quantity'],
            'category'=>$_POST['category'],
            'tags'=>$_POST['tags']
        );
        $replaceCriteria = [ // Get ID from the URL
            "_id" => new MongoDB\BSON\ObjectID($_GET['id'])
        ];
        $updateResult = $collection->replaceOne($replaceCriteria, $replace); // Update product
        if($updateResult->getModifiedCount()==1){
            echo 'Product edited.';
        }
        else {
            echo 'Error editing product';
        }
    }
    outputFooter();
?>