<?php
    include('libs/common.php'); 
	outputCMSNav();
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient =(new MongoDB\Client);
    $db =$mongoClient-> ecommerce;
    $tableObj =$db->Products->find(); // Get all products
?>

<div class="cms-content">
    <a href="cms-add.php">Add new product</a>
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Category</th>
            <th>Price</th>
            <th>Weight</th>
            <th>Quantity</th>
            <th>Tags</th>
            <th></th>
            <th></th>
        </tr>
        <?php
            foreach ($tableObj as $row) { /*display the collection on the table*/
        ?>
        <tr>
            <td><?php echo $row['_id']?></td>
            <td><?php echo $row['name']?></td>
            <td><?php echo $row['category']?></td>
            <td><?php echo $row['price']?></td>
            <td><?php echo $row['weight']?></td>
            <td><?php echo $row['quantity']?></td>
            <td><?php echo $row['tags']?></td>
            <td><a href=<?php echo "cms-edit.php?id=" . $row['_id']?>>Edit product</a></td>
            <td><a href="#">Delete product</a></td>
        </tr>
        <?php 
            }
        ?>
       
    </table>
</div>

<script>
    if(sessionStorage.loggedInUsr == undefined) { // Check if user is logged in
        window.location.replace("cms-login.php");
    }
</script>

<?php
    outputFooter();
?>