<?php
    // Registration server
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient =(new MongoDB\Client);
    $db =$mongoClient-> ecommerce;
    $collection =$db->Customer;
    if($_POST)
    {
        $insert=  array(
            'full_name'=>$_POST['name'],
            'email'=>$_POST['email'],
            'phone_number'=>$_POST['phone'],
            'postcode'=>$_POST['postcode'],
            'address'=>$_POST['address'],
            'password'=>$_POST['password']
        );
        $insertResult = $collection->insertOne($insert);
        if($insertResult->getInsertedCount()==1) { // Send user back to the main page
            header("Location: index.php");
            exit();
        }
        else {
            echo 'Error adding customer';
        }
    }
?>