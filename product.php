<?php
    include('libs/common.php'); 
	outputHeaderNav();
?>

<!-- Product page example  -->
<div class="content">
    <h1>Placeholder</h1>
    <img
            class="product-pic"
            src="img/placeholder.jpeg"
            alt="bread"
            width="200"
            height="200"
        />
    <p class="product-category">Bread</p>
    <p class="product-price">Price: £1.99</p>
    <p class="product-weight">Weight: 1kg</p>
    <p class="product-quantity">Quantity available: 500</p>
    <a href="#">Add to basket</a>
</div>

<?php
    outputFooter();
?>