<?php
    include('libs/common.php'); 
	outputHeaderNav();
?>

<div class="content">
    <!-- Basket -->
    <div id="basket" class="products-basket">
        <h2>Your basket:</h2>
        <div class="product" href="#">
            <a href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            </a>
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
            <p class="product-quantity">100</p>
            <a href="#">Delete</a>
        </div>
        <p>Total: £199</p>
        <a href="#">Checkout</a>
    </div>
</div>

<?php
    outputFooter();
?>