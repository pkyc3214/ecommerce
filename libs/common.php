<?php
function outputHeaderNav() {//Outputs head tag, opening body and main div tags, and the navigation menu for shop pages
    echo '<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8" />
            <meta http-equiv="X-UA-Compatible" content="IE=edge" />
            <meta name="viewport" content="width=device-width, initial-scale=1.0" />
            <title>Ecommerce project</title>
            <link rel="Stylesheet" href="css/style.css" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0-beta2/css/all.min.css">
        </head>
        <body>
            <div id="main">
            <nav class="navmenu">
                <a href="index.php"
                ><img
                    id="logo"
                    src="img/logo.png"
                    alt="logo"
                    width="50"
                    height="30"
                /></a>
                <ul>
                <li>
                    <div class="dropdown">
                        <a class="dropa" href="products.php">Products</button>
                        <div class="dropdown-content">
                            <a href="products.php">Bread</a>
                            <a href="products.php">Rolls</a>
                            <a href="products.php">Other</a>
                        </div>
                    </div>
                </li>
                <li><a href="about.php">About us</a></li>
                </ul>
                <div class="search">
                    <form action="#">
                        <input type="text" placeholder="Search" name="search">
                        <button type="submit"><i class="fa fa-search"></i></button>
                    </form>
                </div>
                <div id="menu-right">
                    <div id="login-register">
                    <a href="login.php">Login</a>
                    <a href="register.php">Register</a>
                    <a href="basket.php">Basket</a>
                </div>
                </div>
            </nav>';
}

function outputFooter() {//Outputs website footer and closing body and html tags
    echo '<footer class="footer">
    <div class="container-footer">
       
       <div >
       <ul>
       <li class="quick-items"> <a href="#">About us</a></li>
       <li class="quick-items"> <a href="#">Contact  us</a></li>
         
       </ul>
       <div class="social_links">
       <ul>
       <li class="social-items"><a href="#"><i class="fab fa-facebook-f"> </i></a></li>
       <li class="social-items"> <a href="#"><i class="fab fa-twitter"> </i></a></li>
       <li class="social-items"> <a href="#"><i class="fab fa-instagram"> </i></a></li>
       </ul>
       </div>
        
    </div>
</div>
           
           
        </footer>
       ';
}

function outputCMSNav() {//Outputs head tag, opening body and main div tags, and the navigation menu for CMS pages
    echo '<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Ecommerce project</title>
        <link rel="Stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div id="main">
        <nav class="cms-navmenu">
        <ul>
            <li><a href="cms-products.php">View products</a></li>
            <li><a href="cms-orders.php">View orders</a></li>
            <li><a href="cms-users.php">View customers</a></li>
        </ul>
        </nav>';
}

function outputBare() { //Outputs head tag, opening body and main div tags
    echo '<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Ecommerce project</title>
        <link rel="Stylesheet" href="css/style.css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    <body>
        <div id="main">';
}