<?php
    include('libs/common.php'); 
	outputHeaderNav();
?>

<div class="content">
    <div id="banner">
        <h1>WELCOME</h1>
    </div>
    <!-- Products page -->
    <div id="products" class="products-featured">
        <h2>Our products:</h2>
        <!-- Sorting settings -->
        <form>
            <label for="sort-by">Sort by:</label>
            <select name="sort-by" id="sort-by">
                <option value="name">Name A-Z</option>
                <option value="price-des">Price descending</option>
                <option value="price-asc">Price ascending</option>
                <option value="popularity">Popularity</option>
            </select>
        </form>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
    </div>
</div>

<?php
    outputFooter();
?>