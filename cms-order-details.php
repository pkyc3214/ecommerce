<?php
    include('libs/common.php'); 
    outputCMSNav();
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient =(new MongoDB\Client);
    $db =$mongoClient-> ecommerce;
    $ordersObj = $db->Order->findOne(['_id' => new MongoDB\BSON\ObjectId($_GET['id'])]); // Get order from database based on the ID in the URL
    $userObj = $db->Customer->findOne(['_id' => new MongoDB\BSON\ObjectId($ordersObj['customer_id'])]); // Get customer from the order object
?>

<!-- Order details -->
<div class="cms-content">
    <p>ID: <?php echo $userObj['_id'] ?></p>
    <p>User: <?php echo $userObj['full_name'] ?></p>
    <p>User e-mail: <?php echo $userObj['email'] ?></p>
    <p>User phone number: <?php echo $userObj['phone_number'] ?></p>
    <p>Address: <?php echo $userObj['address'] ?></p>
    <p>Time: <?php echo $ordersObj['time'] ?></p>
    <p>Date: <?php echo $ordersObj['date'] ?></p>
    <table>
        <tr>
            <th>No.</th>
            <th>Product ID</th>
            <th>Product name</th>
            <th>Price</th>
            <th>Quantity</th>
        </tr>
        <tr>
            <td>1</td>
            <td>1</td>
            <td>Placeholder</td>
            <td>£1.99</td>
            <td>100</td>
        </tr>
    </table>
    <p>Total: £199</p>
</div>

<script>
    if(sessionStorage.loggedInUsr == undefined) { // Check if user is logged in
        window.location.replace("cms-login.php");
    }
</script>


<?php
    outputFooter();
?>