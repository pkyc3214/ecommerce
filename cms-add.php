<?php
    include('libs/common.php'); 
	outputCMSNav();
?>

<div class="cms-content">
    <div id="banner">
        <h1>ADD PRODUCT</h1>
    </div>
    <!-- New product form -->
    <div class="form">
        <form action="add_product_database.php" method = post>
            <label for="name">Product name</label><br />
            <input type="text" id="name" name="name" /><br /><br />
            <label for="pic">Product picture</label><br />
            <input type="file" id="pic" name="pic"><br /><br /> 
            <label for="weight">Weight (in kilograms)</label><br />
            <input type="number" id="weight" name="weight" /><br /><br />
            <label for="price">Price (in pounds)</label><br />
            <input type="number" id="price" name="price" /><br /><br />
            <label for="quantity">Quantity</label><br />
            <input type="number" id="quantity" name="quantity" /><br /><br />
            <label for="category">Category</label><br />
            <select name="category" id="category">
                <option value="bread">Bread</option>
                <option value="rolls">Rolls</option>
                <option value="other">Other</option>
            </select><br /><br />
            <label for="tage">Product tags</label><br />
            <input type="text" id="tags" name="tags" /><br /><br />
            <input id="butt" type="submit" value="Add" /><br /><br />
        </form>
    </div>
</div>

<script>
    if(sessionStorage.loggedInUsr == undefined) { // Check if user is logged in
        window.location.replace("cms-login.php");
    }
</script>

<?php
    outputFooter();
?>