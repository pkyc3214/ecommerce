<?php
    // Add product server
    include('libs/common.php'); 
    outputCMSNav();
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient =(new MongoDB\Client);
    $db =$mongoClient-> ecommerce;
    $collection =$db->Products;
    if($_POST)
    {
        $insert=  array(
            'name'=>$_POST['name'],
            'weight'=>$_POST['weight'],
            'price'=>$_POST['price'],
            'quantity'=>$_POST['quantity'],
            'category'=>$_POST['category'],
            'tags'=>$_POST['tags']
        );
        $insertResult = $collection->insertOne($insert);
        if($insertResult->getInsertedCount()==1) {
            echo 'Product added.';
            echo'<br>';
            echo ' New document id: ' . $insertResult->getInsertedId();
        }
        else {
            echo 'Error adding product';
        }
    }
    outputFooter();
?>