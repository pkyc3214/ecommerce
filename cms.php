<?php
    include('libs/common.php'); 
	outputCMSNav();
?>

<!-- CMS main page -->
<div class="cms-content">
    <h1>WELCOME</h1>
    <h2>Please choose action from the menu</h2>
</div>

<script>
    if(sessionStorage.loggedInUsr == undefined) { // Check if user is logged in
        window.location.replace("cms-login.php");
    }
</script>
<?php
    outputFooter();
?>