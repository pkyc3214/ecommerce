<?php
    include('libs/common.php'); 
	outputCMSNav();
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient =(new MongoDB\Client);
    $db =$mongoClient-> ecommerce;
    $customerObj =$db->Customer->find(); // Get all users
?>

<!-- Users list -->
<div class="cms-content">
    <a href="cms-add.php">Add new product</a>
    <table>
        <tr>
            <th>ID</th>
            <th>Full name</th>
            <th>E-mail address</th>
            <th>Phone number</th>
            <th>Address</th>
            <th>Post code</th>
            <th></th>
        </tr>
        <?php
            foreach ($customerObj as $row) {
        ?>
        <tr>
             <td><?php echo $row['_id']?></td>
            <td><?php echo $row['full_name']?></td>
            <td><?php echo $row['email']?></td>
            <td><?php echo $row['phone_number']?></td>
            <td><?php echo $row['address']?></td>
            <td><?php echo $row['postcode']?></td>
            <td><a href="#">Delete user</a></td>
        </tr>
        <?php 
            }
        ?>
    </table>
</div>

<script>
    if(sessionStorage.loggedInUsr == undefined) { // Check if user is logged in
        window.location.replace("cms-login.php");
    }
</script>

<?php
    outputFooter();
?>