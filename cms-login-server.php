<?php
    // CMS login server
    $email= filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING);
    $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
    if($email != "" && $password != "") {
        require __DIR__ . '/vendor/autoload.php';
        $mongoClient =(new MongoDB\Client);
        $db =$mongoClient-> ecommerce;
        $adminObj = $db->admin->findOne(['email' => $email]); // Find user
        if($adminObj['password'] == $password) { // Check if password inputted matches the password in the database
            echo "ok";
        }
        else {
            echo "not ok";
        }
    }
    else {
        echo "not ok";
    }
?>