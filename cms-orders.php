<?php
    include('libs/common.php'); 
    outputCMSNav();
    require __DIR__ . '/vendor/autoload.php';
    $mongoClient =(new MongoDB\Client);
    $db =$mongoClient-> ecommerce;
    $ordersObj =$db->Order->find(); // Get all orders
?>
<!-- Current orders list -->
<div class="cms-content">
    <table>
        <tr>
            <th>ID</th>
            <th>User e-mail</th>
            <th>Shipping Address</th>
            <th>Total</th>
            <th>Date and time</th>
            <th></th>
            <th></th>
        </tr>
        <?php
            foreach ($ordersObj as $row) {
        ?>
        <tr>
            <td><?php echo $row['_id']?></td>
            <td><?php echo $row['email']?></td>
            <td><?php echo $row['shipping_address']?></td>
            <td><?php echo $row['cost']?></td>
            <td><?php echo $row['date'],$row['time']?></td>
            <td><a href=<?php echo "cms-order-details.php?id=" . $row['_id']?>>Details</a></td>
            <td><a href=<?php echo "delete_order_database.php?id=" . $row['_id']?>>Delete</a></td>
        </tr>
        <?php 
            }
        ?>
    </table>
</div>

<script>
    if(sessionStorage.loggedInUsr == undefined) { // Check if user is logged in 
        window.location.replace("cms-login.php");
    }
</script>

<?php
    outputFooter();
?>