<?php
    include('libs/common.php'); 
	outputBare();
?>

<!-- CMS login -->
<div class="content">
    <div class="form">
        <p>
            <label for="email">E-mail address</label><br />
            <input type="email" id="email" name="email" /><br /><br />
            <label for="password">Password</label><br />
            <input type="password" id="password" name="password" /><br /><br />
            <input onclick="login()" id="butt" type="submit" value="Login" /><br /><br />
        </p>
    </div>
</div>

<script>
    function login() {
        let request = new XMLHttpRequest();
        request.onload = () => {
            if(request.status === 200) {
                let responseData = request.responseText;
                if (responseData == "ok") { // If user exists and password matches, log him in
                    alert("Logged in.");
                    sessionStorage.loggedInUsr = document.getElementById("email").value;
                    window.location.replace("cms.php");
                }
                else {
                    alert("Email or password incorrect. Please try again.");
                }
            }
            else
                alert("Error communicating with server: " + request.status);
        };
        request.open("POST", "cms-login-server.php");
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        // Get user email and password from the form
        let usrMail = document.getElementById("email").value; 
        let usrPassword = document.getElementById("password").value;
        request.send("email=" + usrMail + "&password=" + usrPassword); 
    }
</script>

<?php
    outputFooter();
?>