<?php
    include('libs/common.php'); 
	outputHeaderNav();
?>

<div class="content">
    <div id="banner">
        <h1>LOGIN</h1>
    </div>
    <!-- Login form -->
    <div class="form">
        <form>
            <label for="email">E-mail address</label><br />
            <input type="email" id="email" name="email" /><br /><br />
            <label for="password">Password</label><br />
            <input type="password" id="password" name="password" /><br /><br />
            <input id="butt" type="submit" value="Login" /><br /><br />
            <a href="register.php">Register</a>
        </form>
    </div>
</div>

<?php
    outputFooter();
?>