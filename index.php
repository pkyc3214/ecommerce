<?php
    include('libs/common.php'); 
	outputHeaderNav();
?>

<div class="content">
    <div id="banner">
        <h1>WELCOME</h1>
    </div>
    <!-- Recommended products -->
    <div id="recommended" class="products-featured">
        <h2>Recommended products:</h2>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a href="products.php">More products</a>
    </div>
    <!-- Popular products -->
    <div id="popular" class="products-featured">
        <h2>Popular products:</h2>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a class="product" href="#">
            <img
                class="product-pic"
                src="img/placeholder.jpeg"
                alt="bread"
                width="100"
                height="100"
            />
            <p class="product-name">Placeholder</p>
            <p class="product-price">£1.99</p>
        </a>
        <a href="products.php">More products</a>
    </div>
    
</div>

<?php
    outputFooter();
?>