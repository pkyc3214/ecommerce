<?php
    include('libs/common.php'); 
	outputHeaderNav();
?>

<div class="content">
    <div id="banner">
        <h1>REGISTER</h1>
    </div>
    <!-- Registration form -->
    <div class="form">
        <form action="add_customer_database.php" method = post>
            <label for="name">Full name</label><br />
            <input type="text" id="name" name="name" /><br /><br />
            <label for="email">E-mail address</label><br />
            <input type="email" id="email" name="email" /><br /><br />
            <label for="phone">Phone number</label><br />
            <input type="tel" id="phone" name="phone" /><br /><br />
            <label for="address">Address</label><br />
            <input type="text" id="address" name="address" /><br /><br />
            <label for="postcode">Postcode</label><br />
            <input type="text" id="postcode" name="postcode" /><br /><br />
            <label for="password">Password</label><br />
            <input type="password" id="password" name="password" /><br /><br />
            <input id="butt" type="submit" value="Register" /><br /><br />
        </form>
    </div>
</div>

<?php
    outputFooter();
?>